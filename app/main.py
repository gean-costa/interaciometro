import argparse
import matplotlib.pyplot as plt
from tabulate import tabulate
from coletar_dados import get_tweets, get_likes
from processar_dados import top_users_likes, top_users_replies, top_users_retweets, score, tweets_per_hour


def main():
    parser = argparse.ArgumentParser(prog='get_tweets.py')

    parser.add_argument(
        '--username', type=str, help='Username do perfil, acompanhado do @', required=True)
    parser.add_argument(
        '--num_users', type=int, help='Número de usuários presentes no ranking', default=20)

    args = parser.parse_args()

    tweets = get_tweets(username=args.username)
    likes = get_likes(username=args.username)

    likes_df = top_users_likes(likes)
    index = likes_df['user'] == args.username
    likes_df = likes_df.drop(likes_df.index[index])
    likes_tb = tabulate(likes_df[0:args.num_users].to_numpy(), headers=[
        'user', 'num_likes'], tablefmt="pretty")
    print(likes_tb)

    replies_df = top_users_replies(tweets)
    index = replies_df['user'] == args.username
    replies_df = replies_df.drop(replies_df.index[index])
    replies_tb = tabulate(replies_df[0:args.num_users].to_numpy(), headers=[
        'user', 'num_replies'], tablefmt="pretty")
    print(replies_tb)

    retweets_df = top_users_retweets(tweets)
    index = retweets_df['user'] == args.username
    retweets_df = retweets_df.drop(retweets_df.index[index])
    retweets_tb = tabulate(retweets_df[0:args.num_users].to_numpy(), headers=[
        'user', 'num_retweets'], tablefmt="pretty")
    print(retweets_tb)

    interactions_df = score(likes_df, replies_df, retweets_df)
    interactions_tb = tabulate(interactions_df[0:args.num_users].to_numpy(),
                               headers=['user', 'num_likes', 'num_replies',
                                        'num_retweets', 'score'],
                               tablefmt="pretty")
    print(interactions_tb)

    tweets_freq_df, tweets_freq_tb = tweets_per_hour(tweets)
    print(tweets_freq_tb)
    tweets_freq_df.plot.bar(x='hour', y='count')
    plt.show()


if __name__ == "__main__":
    main()
